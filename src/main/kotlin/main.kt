package com.sandbox

fun main() {
    print("Hello world")
}

class Calculator {
    /**
     *  @param a первое слагаемое int
     *  @param b второе слагаемое int
     *  @return integer (sum of a and b)
     */
    fun add(a: Int, b: Int): Int {
        return a + b
    }
}
