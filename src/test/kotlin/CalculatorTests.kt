package com.sandbox

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CalculatorTests {

    @Test
    fun `2 + 2 = 4`() {
        val calculator = Calculator()
        assertEquals(4, calculator.add(2, 2), "2 + 2 should equal 4")
    }
}
